import tensorflow as tf
from create_simple_data import _create_samples

num_samples_per_class_training = 1000
num_samples_per_class_test = 100

#without noise, we should get 100% accuracy, with noise (sigma = 0.4) about 85%
with_noise = False
training_features, training_labels = _create_samples(1, num_samples_per_class_training, with_noise, True, False)
test_features, test_labels = _create_samples(1, num_samples_per_class_test, with_noise, True, False)

print(training_features)


#setup the model
model = tf.keras.Sequential()
#model.add(tf.keras.layers.InputLayer(input_shape=(4, 1))) # todo: need to determine the correct format
#model.add(tf.keras.layers.Dense(20, activation='relu'))
model.add(tf.keras.layers.Dense(4, activation='softmax'))



model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate = 0.1),
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['sparse_categorical_accuracy'])


model.fit(training_features, training_labels, epochs=10)
model.evaluate(test_features, test_labels)

model.summary()
