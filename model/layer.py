import numpy as np
from activation.functions import activation_functions

class Layer(object):

    def __init__(self, inputs: int, neurons: int, activation: str):
        sigma = 2 / inputs; # he initalization
        self.W = sigma * np.random.randn(neurons, inputs)
        
        self.b = sigma * np.random.randn(neurons, 1)
        self.activation = activation
        act = activation_functions.get(activation)
       
        self.act = act[0]
        self.d_act = act[1]

        self.initialW = self.W
        self.initialb = self.b

    def __repr__(self):
        return "\nLayer:(neurons: {}\n inputs: {}\n activation function: {}\n initial weights: {}\nfinal weights: {}\n initial biases: {}\nfinal biases: {})".format(np.size(self.W, 0), np.size(self.W, 1), self.activation, self.initialW, self.W, self.initialb, self.b)

    def __str__(self):
        return self.__repr__()

    def __len__(self):
        return np.size(self.W, 0)

    # Feedforward pass.
    # The method takes output values of a previous layer (or the input values of the network) and applies
    # the transfer function (sum of the product between weights and input values).
    # It then applies the activation function (softmax, ReLu, Sigmoid, ...).
    def feedforward(self, A_prev):
        self.A_prev = A_prev
        self.Z = np.dot(self.W, self.A_prev) + self.b
        # Normalize the input from the previous layer to avoid buffer overflow in the exponential function.
        self.Z = (self.Z - np.mean(self.Z)) / np.std(self.Z)
        self.A = self.act(self.Z)
        return self.A

    # Backpropagation.
    # This method takes in an error (dA) and a learning rate (learning_rate).
    # After calculating and applying the weight adjustments it propagates the error back to the previous layer.
    def optimize(self, dA, learning_rate):
        # dods is the derivative of the activation function with respect to the transfer function.
        dods = self.d_act(self.Z)                #do/ds : mxm for Softmax or mx1 for ReLu.

        # drdo is the derivative of the loss function with respect to the activation function.
        # if we are at a hidden layer, then the passed dA is already self.W.T * dA
        drdo = dA                                #dr/do : mx1

        # dods_drdo is the product of dods and drdo
        dods_drdo = 0

        # We need to discern between vector- and matrix-valued input
        if dods.shape == drdo.shape: # ReLu, etc. which return a vector as output
            dods_drdo = np.multiply(dods,drdo)
        else:                      # Softmax, which returns a matrix as output
            dods_drdo =  np.dot(dods, drdo)  # do/ds * dr/do

        # dW is the change of the weights
        dW = 1/dA.shape[0] * np.dot(dods_drdo, self.A_prev.T)     # o * do/ds * dr/do : mx1 * 1xn = mxn 
        # db is the change of the bias terms
        db = 1/dA.shape[0] * dods_drdo #  mx1

        # dA_prev is the error which gets backpropagated to the previous layer
        dA_prev = np.dot(self.W.T, dods_drdo)  # dr/do <- W * do/ds * dr/do

        # updating weights and bias terms
        self.W = self.W - learning_rate * dW
        self.b = self.b - learning_rate * db

        # Normalize both weights and biases in order to avoid them to get too large,
        # which could lead to a buffer overflow in softmax in the next feedforward pass.
        self.W = (self.W - np.mean(self.W)) / np.std(self.W)
        self.b = (self.b - np.mean(self.b)) / np.std(self.b)

        # Flag for printing the internal values like the intermediate weights, calculated during backpropagation
        _printValues = False
        if _printValues:
            print("dods: ", dods)
            print("drdo: ", drdo)
            print("dods_drdo: ", dods_drdo)
            print("activation:", self.activation)
            print("dW:", dW)
            print("db:", db)

        return dA_prev
