import numpy as np

# The function creates a simple test data set
# parameters:
# class_offsett: the offsett that the respective class feature should have
# num_samples_per_class: the number of samples generated for each class. The total number of samples will be three times as much
# add_noise: flag for adding noise to the data
# normalize: flag for normalizing the data in the range between 0 and 1 (recommended)
# add_additional_dimension: flag for making the data compatible with processing of the MNIST data set. When using this data in Tensorflow, the flag needs to be set to False
def _create_samples(class_offsett = 1, num_samples_per_class = 1000, add_noise = True, normalize = True, add_additional_dimension = True):
    # create matrices with 4 features to discern 3 classes
    # class 1 will have features [class_offsett+noise, 0+noise, 0+noise, 0+noise]
    # class 2 will have features [0+noise, class_offsett+noise, 0+noise, 0+noise
    # class 3 will have features [0+noise, 0+noise, class_offsett+noise, 0+noise]
    sigma = 0.5
    zero_offset = 0.01 # in order to avoid overflows when using divide
    if add_noise == True:
        class1_features = np.array(
            [class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])
        class2_features = np.array(
            [zero_offset + sigma * np.random.randn(), class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])
        class3_features = np.array(
            [zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])

        for i in range(1, num_samples_per_class):
            class1_features = np.vstack([class1_features, np.array(
                [class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])])
            class2_features = np.vstack([class2_features, np.array(
                [zero_offset + sigma * np.random.randn(), class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])])
            class3_features = np.vstack([class3_features, np.array(
                [zero_offset + sigma * np.random.randn(), zero_offset + sigma * np.random.randn(), class_offsett + sigma * np.random.randn(), zero_offset + sigma * np.random.randn()])])
    else:
        class1_features = np.array(
            [class_offsett, zero_offset, zero_offset, zero_offset])
        class2_features = np.array(
            [zero_offset, class_offsett, zero_offset, zero_offset])
        class3_features = np.array(
            [zero_offset, zero_offset, class_offsett, zero_offset])

        for i in range(1, num_samples_per_class):
            class1_features = np.vstack([class1_features, np.array(
                [class_offsett, zero_offset, zero_offset, zero_offset])])
            class2_features = np.vstack([class2_features, np.array(
                [zero_offset, class_offsett, zero_offset, zero_offset])])
            class3_features = np.vstack([class3_features, np.array(
                [zero_offset, zero_offset, class_offsett, zero_offset])])

    features = np.vstack([class1_features, class2_features, class3_features])


    if normalize == True:
        fm = np.mean(features)
        fsd = np.std(features)
        for i in range (0, features.shape[0]-1):
            # min-max normalization: y = (x - min) / (max - min)
            features[i, :]  = (features[i, :] - np.amin(features[i, :])) / (np.amax(features[i, :]) - np.amin(features[i, :]))
            # standardization: y = (x-mean)/standard_deviation
           # features[i, :] = (features[i, :] - fm) / fsd



            #print(features[i, :])


    # add another dimension to be compatible with the format of flat_images
    if add_additional_dimension == True:
        features = features[:, :, np.newaxis]


    labels_class1 = np.repeat(0, num_samples_per_class)
    labels_class2 = np.repeat(1, num_samples_per_class)
    labels_class3 = np.repeat(2, num_samples_per_class)

    labels = np.concatenate((labels_class1, labels_class2, labels_class3))

    return features, labels