import numpy as np
CONST_VERRY_SMALL=10**(-50)

def noact(x):
    return x

def d_noact(x):
    for i in range(0, len(x)):
        x[i] = 1
    return x

# x is the vector of input values (sum of weights * output of previous layer)
def softmax(x):
    ### STUDENT TODO START ###
    # Return the result of the softmax function.
    # The output vector should have the same size as the input vector.
    # Be careful to check the sum of the exponential function against zero in order to avoid division by zero.
    # If it is zero, return 0.

    ex = np.exp(x)
    sumex = ex.sum()
    #sumex = max(ex.sum(), CONST_VERY_SMALL)
    if sumex == 0:
        return 0
    else:
        return ex / sumex
    ### STUDENT TODO END ###

# x is the vector of input values (sum of weights * output of previous layer)
def d_softmax(x):
    ### STUDENT TODO START ###
    # Define the derivative of the softmax.
    # Caution: For the softmax function, you need to discern the two cases i == l and i != l
    # Hence, the returning variable should be a square matrix:
    # derivative[i][l], instead of a one dimensional vector. The matrix should have the size x.size * x.size
    # Be careful to check the sum of the exponential function against zero in order to avoid division by zero.
    # If it is zero, the returned matrix should contain only 0 elements.

    ex = np.exp(x)
    #sumex = max(ex.sum(), CONST_VERY_SMALL
    sumex = ex.sum()
    sumex_sq = sumex*sumex
    #derivative = np.zeros((x.shape[0], x.shape[0]))
    derivative = np.zeros((x.size, x.size))
    if sumex != 0:
        for i in range(x.shape[0]):
            for j in range(x.shape[0]):
                if i == j:
                    derivative[i][j] += ex[i] / sumex * (1 - ex[i] / sumex)
                else:
                    derivative[i][j] += (-ex[i] * ex[j]) / sumex_sq
    return derivative

    ### STUDENT TODO END ###


# x is the vector of input values (sum of weights * output of previous layer)
def relu(x):
    ### STUDENT TODO START ###
    # Return the result of the ReLu function.
    # The output vector should have the same size as the input vector.
    p=x
    for i in range(0, len(x)):
        p[i]=max(0, x[i])
    return p
    ### STUDENT TODO END ###

# x is the vector of input values (sum of weights * output of previous layer)
def d_relu(x):
    ### STUDENT TODO START ###
    # Return the result of the derivative of the ReLu function.
    # The output vector should have the same size as the input vector.
    p = x
    for i in range(0, len(x)):
        if x[i] > 0:
            p[i] = 1
        else:
            p[i] = 0
    return p
    ### STUDENT TODO END ###


# x is the vector of input values (sum of weights * output of previous layer)
def sigmoid(x):
    return 1/(1 + np.exp(-x))


# x is the vector of input values (sum of weights * output of previous layer)
def d_sigmoid(x):
    return (1 - sigmoid(x)) * sigmoid(x)



def tanh(x):
    return np.tanh(x)


def d_tanh(x):
    return 1 - np.square(np.tanh(x))


activation_functions = {
    'softmax': [softmax, d_softmax],
    'relu': [relu, d_relu],
    'tanh': [tanh, d_tanh],
    'sigmoid': [sigmoid, d_sigmoid],
    'noact': [noact, d_noact]
}
