# https://www.tensorflow.org/tutorials/load_data/csv
import tensorflow as tf
import pandas as pd
import numpy as np

from create_simple_data import _create_samples

# Make numpy values easier to read.
np.set_printoptions(precision=3, suppress=True)

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing


abalone_train = pd.read_csv(
    "https://storage.googleapis.com/download.tensorflow.org/data/abalone_train.csv",
    names=["Length", "Diameter", "Height", "Whole weight", "Shucked weight",
           "Viscera weight", "Shell weight", "Age"])

#print(abalone_train.head())

abalone_features = abalone_train.copy()
abalone_labels = abalone_features.pop('Age')

abalone_features = np.array(abalone_features)
print(abalone_features.shape)
print(abalone_features)
print(abalone_labels.shape)
print(abalone_labels)

#_create_samples(class_offsett = 1, num_samples_per_class = 1000, add_noise = True, normalize = True, add_additional_dimension = True):

training_features, training_labels = _create_samples(1, 3320, True, True, False)
print(training_features.shape)
print(training_features)
print(training_labels.shape)
print(training_labels)

abalone_model = tf.keras.Sequential([
  layers.Dense(64),
  layers.Dense(1)
])

abalone_model.compile(loss = tf.losses.MeanSquaredError(),
                     optimizer = tf.optimizers.Adam())

#abalone_model.summary()

#abalone_model.fit(abalone_features, abalone_labels, epochs=10)
#abalone_model.fit(training_features, training_labels, epochs=10)
model = tf.keras.Sequential()
#model.add(tf.keras.layers.InputLayer(input_shape=(4, 1)))
model.add(tf.keras.layers.Dense(4, activation='softmax'))
#model.summary()
model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate = 0.1),
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['sparse_categorical_accuracy'])


model.fit(training_features, training_labels, epochs=10)


#
# t = tf.constant([[[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]]])
# print(tf.shape(t))
#
#
#
# m1 = tf.keras.Sequential()
# m1.add(tf.keras.layers.InputLayer(input_shape=(4,)))
# m1.add(tf.keras.layers.Dense(4, activation='softmax'))
#
# m1.summary()
#
# m2 = tf.keras.Sequential()
# m2.add(tf.keras.layers.InputLayer(input_shape=(4,1)))
# m2.add(tf.keras.layers.Dense(4, activation='softmax'))
#
# m2.summary()