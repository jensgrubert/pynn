#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import read
import sys
import numpy as np
from model.model import Model
from input.reader import *

from create_simple_data import _create_samples


def main(argv):
    np.seterr(all='warn')

    num_samples_per_class_training = 300
    num_samples_per_class_test = 50

    with_noise = True
    training_features, training_labels = _create_samples(1, num_samples_per_class_training, with_noise, True, True)
    test_features, test_labels = _create_samples(1, num_samples_per_class_test, with_noise, True, True)

   # print(training_features)
    #print(training_labels)


    print("setting up a simple linear model")

   # nn_linear = Model([
   ## (4, 4, 'relu'),
   # (4, 4, 'softmax')],
   # 'cross_entropy',
   # 0.1, 0.0001, 0.00001)

    nn_linear = Model([
       # (4, 4, 'relu'),
        (4, 4, 'softmax')], #(4, 4, 'noact')],
        'cross_entropy', # 'cross_entropy_softmax', #
        0.1, 0.0001, 0.00001)



    print("training linear model")
    # take all samples for training (batch size = num training samples)
    batch_size = 1 # training_features.shape[0]
    epochs = 10
    nn_linear.train(training_features, training_labels, epochs, batch_size)

    print("after training the model looks like this:")
    print(nn_linear)



    print("evaluating model with training features")

    nn_linear.evaluate(training_features, training_labels)

    print("evaluating model with test features")

    nn_linear.evaluate(test_features, test_labels)

    # A1 = nn_linear.feedforward(training_features[0])
    # print("Estimation:", A1.argmax(), "| Ground Truth:", training_labels[0])
    # print(A1)
    #
    # A2 = nn_linear.feedforward(training_features[num_samples_per_class_training+1])
    # print("Estimation:", A2.argmax(), "| Ground Truth:", training_labels[num_samples_per_class_training+1])
    # print(A2)
    #
    # A3 = nn_linear.feedforward(training_features[2*num_samples_per_class_training + 1])
    # print("Estimation:", A3.argmax(), "| Ground Truth:", training_labels[2*num_samples_per_class_training + 1])
    # print(A3)
    #
    # print("evaluating model with test features")
    # A1t = nn_linear.feedforward(test_features[0])
    # print("Estimation:", A1t.argmax(), "| Ground Truth:", test_labels[0])
    # print(A1t)
    #
    # A2t = nn_linear.feedforward(test_features[num_samples_per_class_test+1])
    # print("Estimation:", A2t.argmax(), "| Ground Truth:", test_labels[num_samples_per_class_test+1])
    # print(A2t)
    #
    # A3t = nn_linear.feedforward(test_features[2*num_samples_per_class_test + 1])
    # print("Estimation:", A3t.argmax(), "| Ground Truth:", test_labels[2*num_samples_per_class_test + 1])
    # print(A3t)


if __name__ == "__main__":
    main(sys.argv[1:])