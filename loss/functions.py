import numpy as np


# assumes that the output layer has no activation function ('none')
def cross_entropy_softmax(y, a):
    ex = np.exp(a)
    sumex = ex.sum()
    sma = a
    if sumex == 0:
        print("WARNING: zero loss")
        return 0
    else:
        sma = ex / sumex
        return np.multiply(y, -np.log(sma))


# assumes that the output layer has no activation function ('none')
def d_cross_entropy_softmax(y, a):
    ex = np.exp(a)
    sumex = ex.sum()
    sma = a
    if sumex == 0:
        print("WARNING: zero loss")
        return 0
    else:
        sma = ex / sumex
        return sma - y

# y is the one hot encoded label vector
# a is the output vector of the network
def cross_entropy_loss(y, a):
    #### STUDENT TODO START ###
    # return the cross entropy loss between y and a
    # hint: this is a one line solution
    # utilize np multiply and np.log

    # ...
    return np.multiply(y, -np.log(a))

    #### STUDENT TODO END ###

# y is the one hot encoded label vector
# a is the output vector of the network
def d_cross_entropy_loss(y, a):
    #### STUDENT TODO START ###
    # return the derivative of the cross entropy loss
    # Be careful to check that all elements of a are non-zero, otherwise, division by zero would occur.
    # If an element of a is zero, the respective element in the output vector also needs to be set to zero.

    # we need to handle the case when the output a == 0
    if np.all(a):  # returns true if no  element is zero
        return -y / a
    else:
        # we need to identify the zero elements. For those the result of the division is set to zero instead of NAN
        res = y
        for i in range(0, y.size):
            if a[i] == 0:
                res[i] = 0
            else:
                res[i] = -y[i] / a[i]
        return res
    #### STUDENT TODO END ###

# y is the one hot encoded label vector
# a is the output vector of the network
def log_loss(y, a):
    return -(y * np.log(a) + (1 - y) * np.log(1 - a))

# y is the one hot encoded label vector
# a is the output vector of the network
def d_log_loss(y, a):
    a[a > 0.99] = 0.99
    return (a - y) / (a * (1 - a))


loss_functions = {
    'cross_entropy': [cross_entropy_loss, d_cross_entropy_loss],
    'log': [log_loss, d_log_loss],
    'cross_entropy_softmax': [cross_entropy_softmax, d_cross_entropy_softmax]
}
