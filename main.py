#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import read
import sys
import numpy as np
from model.model import Model
from input.reader import *


def _flat_images(images, normalize = True):
    flatted = np.zeros((np.size(images, 0), np.size(images, 1) * np.size(images, 2), 1))
    for idx in range(np.size(images, 0)):
        for row in range(np.size(images, 1)):
            for col in range(np.size(images, 2)):
                flatted[idx][row * np.size(images, 1) + col][0] = images[idx][row][col]

    return flatted


def main(argv):

    num_train = 10000
    num_test = 5000

    print("Reading training labels")
    labels_training  = read_labels('res/training/train-labels-idx1-ubyte', num_train)
    print("Reading training images")
    images_training  = read_images('res/training/train-images-idx3-ubyte', num_train)

    print("flattening training images")
    flat_images_training = _flat_images(images_training)

    print("setting up model")
    nn = Model([
    #(784, 128, 'relu'),
    #(128, 10, 'softmax')],
    #(128, 10, 'noact')],
    (784, 10, 'softmax')],
    'cross_entropy',
    0.1, 0.0001, 0.00001)

   # print("before training the model looks like this:")
   # print(nn)

    print("training model")
    nn.train(flat_images_training, labels_training, 10, 1)


    print("evaluating model with training data")
    nn.evaluate(flat_images_training, labels_training)

    print("Reading test labels")
    labels_test = read_labels('res/testing/t10k-labels-idx1-ubyte', num_test)
    print("Reading test images")
    images_test = read_images('res/testing/t10k-images-idx3-ubyte', num_test)

    print("flattening test images")
    flat_images_test = _flat_images(images_test)

    print("evaluating model with test data")
    nn.evaluate(flat_images_test, labels_test)

# A = nn.feedforward(flat_images[0])
   # print("Estimation:", A.argmax(), "| Ground Truth:", labels[0])
   # A = nn.feedforward(flat_images[1])
   # print("Estimation:", A.argmax(), "| Ground Truth:", labels[1])
   # A = nn.feedforward(flat_images[2])
   # print("Estimation:", A.argmax(), "| Ground Truth:", labels[2])
   # A = nn.feedforward(flat_images[3])
   # print("Estimation:", A.argmax(), "| Ground Truth:", labels[3])

   # print("after training the model looks like this:")
   # print(nn)

if __name__ == "__main__":
    main(sys.argv[1:])